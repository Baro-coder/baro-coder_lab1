package tjee.lab1.derby.homework;

import javax.swing.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args){
        String MAIN_TITLE = "Baro-coder_lab1";

        JFrame frame = new JFrame(MAIN_TITLE);
        frame.setContentPane(new MainFrame().panelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
