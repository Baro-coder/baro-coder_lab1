package tjee.lab1.derby.homework;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainFrame {
    private JTextArea outputArea;
    private JButton btnConnect;
    private JButton btnPrint;
    private JButton btnDisconnect;
    private JButton btnDelete;
    private JButton btnAdd;
    public JPanel panelMain;

    String CONNECTED = "CONNECTED";
    String NOT_CONNECTED = "NOT CONNECTED";
    String ALREADY_CONNECTED = "ALREADY CONNECTED";
    String DISCONNECTED = "DISCONNECTED";
    String NOT_DISCONNECTED = "NOT DISCONNECTED";
    String ALREADY_DISCONNECTED = "ALREADY DISCONNECTED";
    String ERROR_PRINT = "GET DATA ERROR";
    String ADDED_RECORD = "NEW RECORD ADDED";
    String NOT_ADDED_RECORD = "NEW RECORD NOT ADDED";
    String DELETED = "ROW DELETTED";
    String ERROR_DELETED = "NO ROW DELETED";


    private boolean connected;
    private String output;

    public MainFrame()
    {
        connected = false;

        btnConnect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnConnectOnClick();
            }
        });

        btnDisconnect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnDisconnectOnClick();
            }
        });

        btnPrint.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnPrintOnClick();
            }
        });

        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnAddOnClick();
            }
        });

        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnDeleteOnClick();
            }
        });
    }

    private void btnConnectOnClick() {
        if(!connected) {
            try {
                DbManager.Connect();
                connected = true;
                outputArea.setText(CONNECTED);
            } catch (SQLException | ClassNotFoundException ex) {
                outputArea.setText(NOT_CONNECTED);
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else{
            outputArea.setText(ALREADY_CONNECTED);
        }
    }
    private void btnDisconnectOnClick() {
        if(connected) {
            try {
                DbManager.Disconnect();
                connected = false;
                outputArea.setText(DISCONNECTED);
            } catch (SQLException ex) {
                outputArea.setText(NOT_DISCONNECTED);
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            outputArea.setText(ALREADY_DISCONNECTED);
        }
    }
    private void btnPrintOnClick() {
        try {
            output = DbManager.GetAllData();
            outputArea.setText(output);
        } catch (SQLException ex) {
            outputArea.setText(ERROR_PRINT);
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    private void btnAddOnClick() {
        if(connected){
            AddFrame dialog = new AddFrame();
            dialog.setTitle("Add new record");
            dialog.pack();
            dialog.setVisible(true);

            if(dialog.isValid){
                String name = dialog.name;
                String field_of_knowledge = dialog.field_of_knowledge;
                String type_of_lesson = dialog.type_of_lesson;
                String teacher = dialog.teacher;
                String way_to_pass = dialog.way_to_pass;

                boolean isElective = dialog.is_elective;
                String is_elective = "";
                if(isElective){
                    is_elective = "true";
                }else{
                    is_elective = "false";
                }

                try{
                    DbManager.AddRecord(name, field_of_knowledge, type_of_lesson, teacher, way_to_pass, is_elective);
                    outputArea.setText(ADDED_RECORD);
                } catch (SQLException ex) {
                    outputArea.setText(NOT_ADDED_RECORD);
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            outputArea.setText(NOT_CONNECTED);
        }
    }

    private void btnDeleteOnClick() {
        if(connected){
            DeleteFrame dialog = new DeleteFrame();
            dialog.setTitle("Delete existing record");
            dialog.pack();
            dialog.setVisible(true);

            if(dialog.isValid){
                try{
                    String id = dialog.lesson_id;
                    DbManager.DeleteRecord(id);
                    outputArea.setText(DELETED);
                } catch (SQLException ex) {
                    outputArea.setText(ERROR_DELETED);
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            outputArea.setText(NOT_CONNECTED);
        }
    }
}
