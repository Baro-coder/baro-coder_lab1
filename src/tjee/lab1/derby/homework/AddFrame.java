package tjee.lab1.derby.homework;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddFrame extends JDialog {
    private JPanel contentPane;
    private JButton btnAdd;
    private JButton btnReturn;
    private JTextField nameTextField;
    private JTextField fieldTextField;
    private JTextField typeTextField;
    private JTextField teacherTextField;
    private JTextField wayTextField;
    private JRadioButton radioIsElectiveTrue;
    private JRadioButton radioIsElectiveFalse;
    private JLabel nameValidationLabel;
    private JLabel fieldValidationLabel;
    private JLabel typeValidationLabel;
    private JLabel teacherValidationLabel;
    private JLabel wayValidationLabel;

    public String name;
    public String field_of_knowledge;
    public String type_of_lesson;
    public String teacher;
    public String way_to_pass;
    public boolean is_elective;

    public boolean isValid;

    public AddFrame() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btnAdd);

        clearValidationLabels();

        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnAddOnClick();
            }
        });
        btnReturn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnReturnOnClick();
            }
        });

        radioIsElectiveTrue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(radioIsElectiveTrue.isSelected()){
                    radioIsElectiveFalse.setSelected(false);
                }
            }
        });
        radioIsElectiveFalse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(radioIsElectiveFalse.isSelected()){
                    radioIsElectiveTrue.setSelected(false);
                }
            }
        });
    }

    private void clearValidationLabels(){
        String EMPTY = "                         ";
        nameValidationLabel.setText(EMPTY);
        fieldValidationLabel.setText(EMPTY);
        typeValidationLabel.setText(EMPTY);
        teacherValidationLabel.setText(EMPTY);
        wayValidationLabel.setText(EMPTY);
    }

    private void validateRequiredFields(){
        String REQUIRED_FIELD_MESSAGE = "* Required field";
        if(name.isEmpty()){
            nameValidationLabel.setText(REQUIRED_FIELD_MESSAGE);
            isValid = false;
        }
        if(field_of_knowledge.isEmpty()){
            fieldValidationLabel.setText(REQUIRED_FIELD_MESSAGE);
            isValid = false;
        }
        if(type_of_lesson.isEmpty()){
            typeValidationLabel.setText(REQUIRED_FIELD_MESSAGE);
            isValid = false;
        }
        if(teacher.isEmpty()){
            teacherValidationLabel.setText(REQUIRED_FIELD_MESSAGE);
            isValid = false;
        }
        if(way_to_pass.isEmpty()){
            wayValidationLabel.setText(REQUIRED_FIELD_MESSAGE);
            isValid = false;
        }
    }

    private void btnAddOnClick(){
        clearValidationLabels();

        name = nameTextField.getText();
        field_of_knowledge = fieldTextField.getText();
        type_of_lesson = typeTextField.getText();
        teacher = teacherTextField.getText();
        way_to_pass = wayTextField.getText();
        is_elective = radioIsElectiveTrue.isSelected();

        isValid = true;

        validateRequiredFields();

        if(isValid){
            dispose();
        }
    }
    private void btnReturnOnClick(){
        dispose();
    }
}
