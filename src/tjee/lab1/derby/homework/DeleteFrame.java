package tjee.lab1.derby.homework;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.lang.Integer.valueOf;

public class DeleteFrame extends JDialog {
    private JPanel contentPane;
    private JButton btnDelete;
    private JButton btnReturn;
    private JTextField idTextField;
    private JLabel idValidationLabel;

    private String EMPTY = "                  ";
    private String REQUIRED_FIELD_MESSAGE = " * Required field";

    public String lesson_id;

    public boolean isValid;

    public DeleteFrame() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btnDelete);

        idValidationLabel.setText(EMPTY);

        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnDeleteOnClick();
            }
        });

        btnReturn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnReturnOnClick();
            }
        });
    }

    private void btnDeleteOnClick() {
        if (!idTextField.getText().isEmpty()) {
            lesson_id = idTextField.getText();
            isValid = true;

        } else {
            isValid = false;
            idValidationLabel.setText(REQUIRED_FIELD_MESSAGE);
        }

        if (isValid) {
            dispose();
        }
    }

    private void btnReturnOnClick(){
        dispose();
    }
}
