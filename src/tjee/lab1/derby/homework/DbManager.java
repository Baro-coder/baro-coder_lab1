package tjee.lab1.derby.homework;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class DbManager {
    public static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    public static final String JDBC_URL = "jdbc:derby:./db/baro-coder_db";
    public static final String QUERY_GET_ALL = "SELECT * FROM LESSONS";
    public static final String QUERY_INSERT_TEMPLATE = "INSERT INTO APP.LESSONS(NAME, FIELD_OF_KNOWLEDGE, TYPE_OF_LESSON, TEACHER, WAY_TO_PASS, IS_ELECTIVE) VALUES(";
    public static final String QUERY_DELETE_TEMPLATE = "DELETE * FROM APP.LESSONS WHERE lesson_id = ";
    private static java.sql.Connection conn;

    private DbManager() { }

    public static boolean Connect() throws ClassNotFoundException, SQLException {
        conn = DriverManager.getConnection(JDBC_URL);
        return conn != null;
    }
    public static boolean Disconnect() throws SQLException {
        if (conn == null) {
            return false;
        } else {
            conn.close();
            return true;
        }
    }

    public static String GetAllData() throws SQLException {
        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(QUERY_GET_ALL);
        ResultSetMetaData rsmd = rs.getMetaData();

        String wiersz = "";
        int colCount = rsmd.getColumnCount();

        for (int i = 1; i <= colCount; i++) {
            wiersz = wiersz.concat(rsmd.getColumnName(i) + " \t| ");
        }

        wiersz = wiersz.concat("\r\n");

        while (rs.next()) {
            for (int i = 1; i <= colCount; i++) {
                wiersz = wiersz.concat(rs.getString(i) + " \t| ");
            }
            wiersz = wiersz.concat("\r\n");
        }

        stat.close();
        return wiersz;
    }

    public static void AddRecord(String name, String field_of_knowledge, String type_of_lesson,
                                 String teacher, String way_to_pass, String is_elective) throws SQLException{
        Statement stat = conn.createStatement();
        String querySet = QUERY_INSERT_TEMPLATE.concat("'").concat(name)
                .concat("','").concat(field_of_knowledge)
                .concat("','").concat(type_of_lesson)
                .concat("','").concat(teacher)
                .concat("','").concat(way_to_pass)
                .concat("','").concat(is_elective)
                .concat("')");
        stat.executeUpdate(querySet);
    }

    public static void DeleteRecord(String lesson_id) throws SQLException{
        Statement stat = conn.createStatement();
        String queryset = QUERY_DELETE_TEMPLATE.concat(lesson_id);
        stat.executeQuery(queryset);
    }
}
